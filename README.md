# Simple image optimization by jpegoptim, optipng, gifsicle.

1. Install `jpegoptim, optipng and gifsicle` on your server.
2. Change some settings in `optimize.sh` and add this file to server.
3. Run optimize.sh. Command: `sh optimize.sh`.
