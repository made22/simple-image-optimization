#!/bin/bash

# Path to dir for optimization
scanDir="/var/www/site.com/path/to/images/";

# Don't optimize images in dirs
ignoreDirs=("/var/www/site.com/path/to/images/tmp/*")

# JPG quality (from 1 to 100).
jpgQuality=80

# PNG compression level (from 1 to 7).
pngCompression=3

ignoreDirsArg=()
for ignoredDir in "${ignoreDirs[@]}"; do
  ignoreDirsArg+="-not -path "*${ignoredDir}" "
done

# jpegoptim jpg optimization
find $scanDir -type f -iregex .*\.jpe?g$ $ignoreDirsArg\
-exec jpegoptim -p -m$jpgQuality --all-progressive -f --strip-all {} \;

# optipng png optimization
find $scanDir -type f -iname "*.png" $ignoreDirsArg\
-exec optipng -strip all -o$pngCompression {} \;

# gifsicle gif optimization
find $scanDir -type f -iname "*.gif" $ignoreDirsArg\
-exec gifsicle --batch -V -O2 {} \;
